package com.tarrk.framework.android.gui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import com.tarrk.framework.android.font.TypefaceUtil;

public class RobotoCondensedEditText extends EditText {

    public RobotoCondensedEditText(Context context) {
        super(context);
        TypefaceUtil.initRobotoCondensed(this);
    }

    public RobotoCondensedEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypefaceUtil.initRobotoCondensed(this);
    }

    public RobotoCondensedEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypefaceUtil.initRobotoCondensed(this);
    }

}

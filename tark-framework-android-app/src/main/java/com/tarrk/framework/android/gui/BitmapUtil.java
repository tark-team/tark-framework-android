package com.tarrk.framework.android.gui;

import android.app.Activity;
import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import com.tarrk.framework.android.debug.LLog;
import com.tarrk.framework.android.io.IOUtil;

import java.io.File;
import java.io.IOException;

/**
 * User: Pavel
 * Date: 19.03.2015
 * Time: 21:19
 */
public class BitmapUtil {

    private final static String TAG = BitmapUtil.class.getSimpleName();

    public static Bitmap drawableToBitmap(Drawable drawable) {
        LLog.e(TAG, "drawableToBitmap");

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;

    }

    /**
     * As a result of that method we have vertical image size like a screen divivded y divider
     *
     * @param a
     * @param image
     * @param divider
     * @return
     */
    public static Bitmap transformImageToScreenSize(Activity a, Bitmap image, int divider) {
        LLog.e(TAG, "transformImageToScreenSize");

        int screenWidth = ScreenUtil.getScreenSize(a).x / divider;
        int screenHeight = ScreenUtil.getScreenSize(a).y / divider;

        /**
         * That block used in production
         */
        image = scaleImageToFitTheScreen(image, screenWidth, screenHeight);
        image = cropToRatio(image, (double) screenWidth / (double) screenHeight);

        /**
         * That block used for testing when we ned save image on each step
         * to check where is the problem
         */
        /*try {
            IOUtil.saveBitmapToFilePng(image, IOUtil.getDataStoragePath(a) + File.separator + "1.jpg");
            image = scaleImageToFitTheScreen(image, screenWidth, screenHeight);
            IOUtil.saveBitmapToFilePng(image, IOUtil.getDataStoragePath(a) + File.separator + "2.jpg");
            image = cropToRatio(image, (double) screenWidth / (double) screenHeight);
            IOUtil.saveBitmapToFilePng(image, IOUtil.getDataStoragePath(a) + File.separator + "3.jpg");
        } catch (IOException e) {
            LLog.e(TAG, "IOException - " + e);
            e.printStackTrace();
        }*/

        return image;
    }

    public static Bitmap cropToRatio(Bitmap image, double widthHeightRatio) {
        LLog.e(TAG, "cropToRatio");

        double pictureRatio;

        //screenRatio = (double) width / (double) height;
        pictureRatio = (double) image.getWidth() / (double) image.getHeight();

        int newHeight = (int) ((double) image.getWidth() / (double) widthHeightRatio);
        int newWidth = (int) ((double) image.getHeight() * (double) widthHeightRatio);

        if (widthHeightRatio > pictureRatio) {
            // cut from bottom
            image = Bitmap.createBitmap(image, 0, 0, image.getWidth(), newHeight);
        } else if (widthHeightRatio < pictureRatio) {
            // cut from left and right
            image = Bitmap.createBitmap(image, (image.getWidth() - newWidth) / 2, 0, newWidth, image.getHeight());
        }

        return image;

    }

    public static Bitmap scaleToScreenSize(Bitmap image, int screenWidth, int screenHeight) {
        LLog.e(TAG, "scaleToScreenSize");
        if (image.getWidth() >= screenWidth && image.getHeight() >= screenHeight) {
            return Bitmap.createScaledBitmap(image, screenWidth, screenHeight, true);
        } else if (image.getWidth() >= screenWidth) {
            return Bitmap.createScaledBitmap(image, screenWidth, (int) (screenWidth * ((float) image.getHeight() / (float) image.getWidth())), true);
        } else if (image.getHeight() >= screenHeight) {
            return Bitmap.createScaledBitmap(image, (int) (screenHeight * ((float) image.getWidth() / (float) image.getHeight())), screenHeight, true);
        } else {
            return image;
        }
    }

    public static void savePicture(Context c, Bitmap bm, String fileName) {
        LLog.e(TAG, "savePicture - " + bm.getWidth() + "*" + bm.getHeight());

        File cachePath = new File(IOUtil.getDataStoragePath(c) + File.separator + fileName + ".png");
        try {
            IOUtil.saveBitmapToFilePng(bm, cachePath);
            LLog.e(TAG, "Stored result bitmap to: " + cachePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap rotateImage(Bitmap image) {

        Matrix matrix = new Matrix();
        matrix.postRotate(90);

        if (image.getHeight() < image.getWidth()) {
            return Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, true);
        }

        return image;

    }

    public static int getWeigth(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return bitmap.getRowBytes() * bitmap.getHeight();
        } else {
            return bitmap.getByteCount();
        }
    }

    public static Bitmap scaleImageToFitTheScreen(Bitmap bitmap, int screenWidth, int screenHeight) {
        LLog.e(TAG, "scaleImageToFitTheScreen");

        float imageRatio = (float) bitmap.getWidth() / (float) bitmap.getHeight();
        float screenRatio = (float) screenWidth / (float) screenHeight;

        if (imageRatio < screenRatio) {
            return Bitmap.createScaledBitmap(
                    bitmap,
                    screenWidth,
                    (screenWidth * bitmap.getHeight()) / bitmap.getWidth(),
                    true);
        } else if (imageRatio > screenRatio) {
            return Bitmap.createScaledBitmap(
                    bitmap,
                    (screenHeight * bitmap.getWidth()) / bitmap.getHeight(),
                    screenHeight,
                    true);
        }

        return bitmap;
    }

    public static Bitmap getQuadraticThumbnail(Bitmap bitmap, int size) {
        /**
         * TODO
         * During the polishing make it cut quadrat from the center
         * (current it cut it from the top
         */
        Bitmap result;
        result = cropToRatio(bitmap, 1);
        result = Bitmap.createScaledBitmap(result, size, size, true);
        return result;
    }

    public static Bitmap createEmptyBitmap() {
        return Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
    }

    public static Bitmap createRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(
                bitmap.getWidth(),
                bitmap.getHeight(),
                Bitmap.Config.ARGB_8888
        );
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Bitmap createQuadraticBitmap(Bitmap sourceBitmap) {

        Bitmap resultBitmap;

        if (sourceBitmap.getWidth() >= sourceBitmap.getHeight()) {
            resultBitmap = Bitmap.createBitmap(
                    sourceBitmap,
                    sourceBitmap.getWidth() / 2 - sourceBitmap.getHeight() / 2,
                    0,
                    sourceBitmap.getHeight(),
                    sourceBitmap.getHeight()
            );
        } else {
            resultBitmap = Bitmap.createBitmap(
                    sourceBitmap,
                    0,
                    sourceBitmap.getHeight() / 2 - sourceBitmap.getWidth() / 2,
                    sourceBitmap.getWidth(),
                    sourceBitmap.getWidth()
            );
        }

        return resultBitmap;
    }

}

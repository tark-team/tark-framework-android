package com.tarrk.framework.android.google.admob;

import android.app.Activity;
import com.tarrk.framework.android.debug.LLog;
import com.tarrk.framework.android.gui.ScreenUtil;

/**
 * User: Pavel
 * Date: 14.07.2015
 * Time: 17:10
 */
public class AdmobUtils {

    private static final String TAG = AdmobUtils.class.getSimpleName();

    public static final int BANNER_HEIGHT_SMALL = 32;
    public static final int BANNER_HEIGHT_MEDIUM = 50;
    public static final int BANNER_HEIGHT_LARGE = 90;

    public static final int SCREEN_HEIGHT_SMALL = 400;
    public static final int SCREEN_HEIGHT_BIG = 720;

    public static int getSmartBannerHeight(Activity a) {
        LLog.e(TAG, "getSmartBannerHeight");
        /**
         * https://developers.google.com/admob/android/banner
         */

        int screenHeight = ScreenUtil.getRealScreenSizeInDp(a).y;
        int bannerHeight;

        if (screenHeight < SCREEN_HEIGHT_SMALL) {
            bannerHeight = BANNER_HEIGHT_SMALL;
        } else if (screenHeight >= SCREEN_HEIGHT_SMALL && screenHeight < SCREEN_HEIGHT_BIG) {
            bannerHeight = BANNER_HEIGHT_MEDIUM;
        } else {
            bannerHeight = BANNER_HEIGHT_LARGE;
        }

        LLog.e(TAG, "getSmartBannerHeight - result - " + ScreenUtil.dpToPx(a, bannerHeight));
        return ScreenUtil.dpToPx(a, bannerHeight);

    }

}

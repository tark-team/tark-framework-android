package com.tarrk.framework.android.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;

import java.util.List;

/**
 * User: Pavel
 * Date: 07.12.2015
 * Time: 13:13
 */
public class NetworkUtil {

    public static boolean isConnected(Context context) {
        NetworkInfo activeNetworkInfo = getConnectivityManager(context).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isConnectedOrConnecting(Context context) {
        NetworkInfo activeNetworkInfo = getConnectivityManager(context).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public static Integer getActiveNetworkType(Context context) {
        NetworkInfo activeNetworkInfo = getConnectivityManager(context).getActiveNetworkInfo();

        if (activeNetworkInfo == null) {
            return null;
        }

        return activeNetworkInfo.getType();
    }

    public static boolean isConnectedToWifiNetwork(Context context) {
        Integer activeNetworkType = getActiveNetworkType(context);
        return activeNetworkType != null && activeNetworkType == ConnectivityManager.TYPE_WIFI;
    }

    public static boolean isConnectedToWifiNetwork(Context context, String bssid) {
        WifiInfo activeNetwork = getWifiNetworkInfo(context);
        return activeNetwork != null &&
                activeNetwork.getBSSID() != null
                && activeNetwork.getBSSID().equals(bssid);
    }

    public static boolean isConnectedToMobileNetwork(Context context) {
        Integer activeNetworkType = getActiveNetworkType(context);
        return activeNetworkType != null && activeNetworkType == ConnectivityManager.TYPE_MOBILE;
    }

    public static List<ScanResult> getScanNearbyNetworksResults(Context context) {
        return getWifiManager(context).getScanResults();
    }

    public static WifiInfo getWifiNetworkInfo(Context context) {
        return getWifiManager(context).getConnectionInfo();
    }

    /*************************************
     * PRIVATE STATIC METHODS
     *************************************/
    private static ConnectivityManager getConnectivityManager(Context context) {
        return (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    private static WifiManager getWifiManager(Context context) {
        return (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    }

    private static TelephonyManager getTelephonyManager(Context context) {
        return (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    }


}

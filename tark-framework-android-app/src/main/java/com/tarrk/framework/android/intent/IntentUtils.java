package com.tarrk.framework.android.intent;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

/**
 * User: Pavel
 * Date: 26.11.2015
 * Time: 15:33
 */
public class IntentUtils {

    public static void openUrl(Context context, String url) {
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }

    public static void openUrl(Context context, int urlResId) {
        openUrl(context, context.getString(urlResId));
    }

    public static void openYoutubeLink(Context context, String videoId) {
        String prefix;
        try {
            prefix = "vnd.youtube:";
        } catch (ActivityNotFoundException ex) {
            prefix = "http://www.youtube.com/watch?v=";
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(prefix + videoId));
        context.startActivity(intent);
    }

    public static void openYoutubeLink(Context context, int videoIdResId) {
        openYoutubeLink(context, context.getString(videoIdResId));
    }

    public static void openGooglePlayPage(Context context) {
        String appPackageName = context.getPackageName();
        String prefix;
        try {
            prefix = "market://details?id=";
        } catch (ActivityNotFoundException ex) {
            prefix = "https://play.google.com/store/apps/details?id=";
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(prefix + appPackageName));
        context.startActivity(intent);
    }

    public static void sendMail(Context context, String recipient, String mailSubject, String mailBody, String dialogTitle) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{recipient});
        i.putExtra(Intent.EXTRA_SUBJECT, mailSubject);
        i.putExtra(Intent.EXTRA_TEXT, mailBody);

        try {
            context.startActivity(Intent.createChooser(i, dialogTitle));
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

}

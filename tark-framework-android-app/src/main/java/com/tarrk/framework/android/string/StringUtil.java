package com.tarrk.framework.android.string;

/**
 * User: Pavel
 * Date: 28.09.2015
 * Time: 16:29
 */
public class StringUtil {

    public static String capitalize(String string) {
        if (string == null || string.equals("")){
            return "";
        }
        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }

}

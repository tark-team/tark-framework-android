package com.tarrk.framework.android.gui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tarrk.framework.android.font.TypefaceUtil;

public class RobotoLightTextView extends TextView {

    public RobotoLightTextView(Context context) {
        super(context);
        TypefaceUtil.initRobotoLight(this);
    }

    public RobotoLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypefaceUtil.initRobotoLight(this);
    }

    public RobotoLightTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypefaceUtil.initRobotoLight(this);
    }

}

package com.tarrk.framework.android.gui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import com.tarrk.framework.android.debug.LLog;

/**
 * User: Pavel
 * Date: 06.05.2015
 * Time: 15:24
 */
public class FragmentUtil {

    private final static String TAG = FragmentUtil.class.getSimpleName();

    private enum AddType {ADD, ADD_WITH_ANIMATION, REPLACE, REPLACE_WITH_ANIMATION}

    public static void replaceFragment(FragmentActivity a, int layoutId, Fragment fragment) {
        showFragment(a, layoutId, fragment, AddType.REPLACE, -1, -1);
    }

    public static void replaceFragment(FragmentActivity a, int layoutId, Fragment fragment, int inAnimationResId, int outAnimationResId) {
        showFragment(a, layoutId, fragment, AddType.REPLACE_WITH_ANIMATION, inAnimationResId, outAnimationResId);
    }

    public static void replaceFragment(Fragment parentFragment, int layoutId, Fragment childFRagment) {
        showFragment(parentFragment, layoutId, childFRagment, AddType.REPLACE, -1, -1);
    }

    public static void addFragment(FragmentActivity a, int layoutId, Fragment fragment, int inAnimationResId, int outAnimationResId) {
        showFragment(a, layoutId, fragment, AddType.ADD_WITH_ANIMATION, inAnimationResId, outAnimationResId);
    }

    public static void addFragment(FragmentActivity a, int layoutId, Fragment fragment) {
        showFragment(a, layoutId, fragment, AddType.ADD_WITH_ANIMATION, -1, -1);
    }

    /*public static <T extends Fragment> T prepare(T fragment, Action1<Bundle> argEditor) {
        Bundle args = new Bundle();
        argEditor.call(args);
        fragment.setArguments(args);
        return fragment;
    }*/

    // ---

    private static void showFragment(Object o, int layoutId, Fragment fragment, AddType addType, int inAnimationResId, int outAnimationResId) {

        String backStateName = fragment.getClass().getName();
        FragmentManager fm;

        if (o instanceof FragmentActivity) {
            fm = ((FragmentActivity) o).getSupportFragmentManager();
        } else if (o instanceof Fragment) {
            fm = ((Fragment) o).getChildFragmentManager();
        } else {
            LLog.e(TAG, "First argument must be FragmentActivity or Fragment");
            return;
        }

        if (!fm.popBackStackImmediate(backStateName, 0)) {
            switch (addType) {
                case REPLACE_WITH_ANIMATION:
                    fm
                            .beginTransaction()
                            .setCustomAnimations(inAnimationResId, outAnimationResId)
                            .replace(layoutId, fragment)
                            .addToBackStack(backStateName)
                            .commit();
                    break;
                case REPLACE:
                    fm
                            .beginTransaction()
                            .replace(layoutId, fragment)
                            .addToBackStack(backStateName)
                            .commit();
                    break;
                case ADD_WITH_ANIMATION:
                    fm
                            .beginTransaction()
                            .setCustomAnimations(inAnimationResId, outAnimationResId)
                            .add(layoutId, fragment)
                            .addToBackStack(backStateName)
                            .commit();
                    break;
                case ADD:
                    fm
                            .beginTransaction()
                            .add(layoutId, fragment)
                            .addToBackStack(backStateName)
                            .commit();
                    break;
            }


        }
    }

}

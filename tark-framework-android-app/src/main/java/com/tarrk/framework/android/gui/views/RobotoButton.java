package com.tarrk.framework.android.gui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import com.tarrk.framework.android.font.TypefaceUtil;

public class RobotoButton extends Button {

    public RobotoButton(Context context) {
        super(context);
        TypefaceUtil.initRoboto(this);
    }

    public RobotoButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypefaceUtil.initRoboto(this);
    }

    public RobotoButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypefaceUtil.initRoboto(this);
    }

}

package com.tarrk.framework.android.format;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * User: Pavel
 * Date: 09.04.2015
 * Time: 12:07
 */
public class FormatUtil {

    public static String thousands(float value) {
        return NumberFormat.getNumberInstance(Locale.US).format(value);
    }

    public static String twoDecimals(float value) {
        return new DecimalFormat("###.##").format(value).replace(",", ".");
    }

    public static String oneDecimal(float value) {
        return new DecimalFormat("###.#").format(value).replace(",", ".");
    }

    public static String convertRussianPhoneNumberToInternationalFormat(String phoneNumber) {
        //LLog.e(TAG, "convertRussianPhoneNumberToInternationalFormat");

        /**
         * TODO:
         * better using https://github.com/googlei18n/libphonenumber
         */

        if (phoneNumber.length() != 11 || !phoneNumber.startsWith("8")) {
            return phoneNumber;
        }

        return "+7" + phoneNumber.substring(1);

    }

    public static int millisToMin(long millis) {
        return (int) (millis / (long) (1000 * 60));
    }

    public static int minToMillis(int minutes) {
        return minutes * (1000 * 60);
    }

    public static String getFormattedTime(Calendar c) {
        /**
         * It can be done with SimpleDateFormat instance,
         * but that way is faster and lighten.
         */
        return c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) + ":" +
                c.get(Calendar.SECOND) + "." + c.get(Calendar.MILLISECOND);
    }

    public static Calendar getCalendarForTimeStamp(long timeStampMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStampMillis);
        return calendar;
    }

    public static int getMinutesCount(long timeStampMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStampMillis);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        if (isFirstHalfOfDay(timeStampMillis)) {
            hour += 12;
        } else {
            hour -= 12;
        }

        return (hour * 60) + minute;
    }

    public static int getMinutesCount(long firstTimeStampMillis, long secondTimeStampMillis) {
        int first = getMinutesCount(firstTimeStampMillis);
        int second = getMinutesCount(secondTimeStampMillis);
        return second - first;
    }

    public static boolean isFirstHalfOfDay(long timeStampMillis) {
        Calendar calendar = getCalendarForTimeStamp(timeStampMillis);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        return hour >= 0 && hour < 12;
    }

    public static String capitalizeEveryWord(String str) {
        String[] words = str.split(" ");
        StringBuilder builder = new StringBuilder();

        for (String word : words) {
            builder.append(Character.toUpperCase(word.charAt(0)))
                    .append(word.substring(1)).append(" ");
        }
        return builder.toString().trim();
    }

    public static boolean isRussian() {
        return Locale.getDefault().getLanguage().contentEquals("ru");
    }
}

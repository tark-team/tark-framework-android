package com.tarrk.framework.android.gui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import com.tarrk.framework.android.font.TypefaceUtil;

public class RobotoCondensedButton extends Button {

    public RobotoCondensedButton(Context context) {
        super(context);
        TypefaceUtil.initRobotoCondensed(this);
    }

    public RobotoCondensedButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypefaceUtil.initRobotoCondensed(this);
    }

    public RobotoCondensedButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypefaceUtil.initRobotoCondensed(this);
    }

}

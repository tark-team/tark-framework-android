package com.tarrk.framework.android.gui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tarrk.framework.android.font.TypefaceUtil;

public class RobotoCondensedLightTextView extends TextView {

    public RobotoCondensedLightTextView(Context context) {
        super(context);
        TypefaceUtil.initRobotoCondensedLight(this);
    }

    public RobotoCondensedLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypefaceUtil.initRobotoCondensedLight(this);
    }

    public RobotoCondensedLightTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypefaceUtil.initRobotoCondensedLight(this);
    }

}

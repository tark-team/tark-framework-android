package com.tarrk.framework.android.social;

import android.app.Activity;
import android.os.Bundle;
import com.tarrk.framework.android.debug.LLog;

/**
 * User: Pavel
 * Date: 17.03.2015
 * Time: 13:25
 */
public class Facebook {

    private final static String TAG = Facebook.class.getSimpleName();

    //private UiLifecycleHelper uiHelper;
    private Activity a;

    public Facebook(Activity a) {
        this.a = a;
        //uiHelper = new UiLifecycleHelper(a, null);
    }

    // ---

    public void init() {
        LLog.e(TAG, "initUiHelper");
    }
    /*
    public void onActivityResult(int requestCode, int resultCode, Intent data, FacebookDialog.Callback callback) {
        uiHelper.onActivityResult(requestCode, resultCode, data, callback);
    }
    */

    public void share(String url) {
        /*
        if (FacebookDialog.canPresentShareDialog(a, FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
            FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(a)
                    .setApplicationName(a.getString(R.string.app_name))
                    .setLink("http://google.com")
                    .setDescription("Test description")
                    .setName("Test name")
                    .build();
            uiHelper.trackPendingDialogCall(shareDialog.present());
        } else {
            publishFeedDialog();
        }
        */

    }

    public void like() {

    }

    public void onResume() {
        //AppEventsLogger.activateApp(a);
        //uiHelper.onResume();
    }

    public void onPause() {
        //AppEventsLogger.deactivateApp(a);
        //uiHelper.onPause();

    }

    public void onDestroy() {
        //uiHelper.onDestroy();
    }

    public void onSaveInstanceState(Bundle outState) {
        //uiHelper.onSaveInstanceState(outState);
    }

    // ---

    private void publishFeedDialog() {
        Bundle params = new Bundle();
        params.putString("name", "Facebook SDK for Android");
        params.putString("caption", "Build great social apps and get more installs.");
        params.putString("description", "The Facebook SDK for Android makes it easier and faster to develop Facebook integrated Android apps.");
        params.putString("link", "https://developers.facebook.com/android");
        params.putString("picture", "https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png");
        /*
        WebDialog feedDialog = new WebDialog.FeedDialogBuilder(a, Session.getActiveSession(), params)
                .setOnCompleteListener(new WebDialog.OnCompleteListener() {

                    @Override
                    public void onComplete(Bundle values, FacebookException error) {
                        if (error == null) {
                            // When the story is posted, echo the success
                            // and the post Id.
                            final String postId = values.getString("post_id");
                            if (postId != null) {
                                Toast.makeText(a, "Posted story, id: " + postId, Toast.LENGTH_SHORT).show();
                            } else {
                                // User clicked the Cancel button
                                Toast.makeText(a, "Publish cancelled", Toast.LENGTH_SHORT).show();
                            }
                        } else if (error instanceof FacebookOperationCanceledException) {
                            // User clicked the "x" button
                            Toast.makeText(a, "Publish cancelled", Toast.LENGTH_SHORT).show();
                        } else {
                            // Generic, ex: network error
                            Toast.makeText(a, "Error posting story", Toast.LENGTH_SHORT).show();
                        }
                    }

                })
                .build();
        feedDialog.show();
        */
    }

}

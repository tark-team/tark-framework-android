package com.tarrk.framework.android.google.billing;

import android.app.Activity;
import android.content.Context;
import com.tarrk.framework.android.debug.LLog;
import com.tarrk.framework.android.google.billing.util.*;

import java.util.ArrayList;

/**
 * User: Pavel
 * Date: 12.02.2015
 * Time: 16:37
 */
public class BillingHelper implements IabHelper.OnIabSetupFinishedListener,
        IabHelper.QueryInventoryFinishedListener {

    /*************************************
     * PRIVATE STATIC VARIABLES
     *************************************/
    private static final String TAG = BillingHelper.class.getSimpleName();
    private static final String SKU_TEST = "android.test.purchased";
    private static final int RC_REQUEST = 10001;

    /*************************************
     * PUBLIC VARIABLES
     *************************************/
    public IabHelper mHelper;

    /*************************************
     * PRIVATE VARIABLES
     *************************************/
    private ArrayList<String> mSkus = new ArrayList<String>();
    private PurchaseListener mPurchaseListener;
    private InventoryListener mInventoryListener;

    /*************************************
     * PUBLIC METHODS
     *************************************/
    public BillingHelper(PurchaseListener purchaseListener, InventoryListener inventoryListener) {
        mPurchaseListener = purchaseListener;
        mInventoryListener = inventoryListener;
    }

    @Override
    public void onIabSetupFinished(IabResult result) {
        LLog.e(TAG, "onIabSetupFinished - " + result.getMessage());

        if (!result.isSuccess()) {
            LLog.e(TAG, "Problem setting up In-app BillingFactory: " + result);
            return;
        }

        mHelper.queryInventoryAsync(true, mSkus, this);
    }

    @Override
    public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
        LLog.e(TAG, "onQueryInventoryFinished");

        if (result.isFailure()) {
            LLog.e(TAG, "onQueryInventoryFinished - Query Inventory fail - " + result.getMessage());
            return;
        }

        for (String s : mSkus) {
            if (s.equals(SKU_TEST)) {
                mInventoryListener.onInventorySuccessful(s, createTestSkuDetails(), inventory.getPurchase(s) != null);
            } else {
                if (inventory.getSkuDetails(s) != null) {
                    mInventoryListener.onInventorySuccessful(s, inventory.getSkuDetails(s), inventory.getPurchase(s) != null);
                } else {
                    mInventoryListener.onInventoryFailed(s);
                }
            }
        }

        mInventoryListener.onInventoryFinished();

    }

    public void addSku(String sku) {
        mSkus.add(sku);
    }

    public void buy(Activity activity, final String skuName) {
        LLog.e(TAG, "buy");
        // TEST
        mHelper.launchPurchaseFlow(activity, skuName, RC_REQUEST, new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                LLog.e(TAG, "onIabPurchaseFinished - " + skuName + ", " + result.toString() + ", " + (purchase != null ? purchase.toString() : "null"));

                if (result.isFailure()) {
                    LLog.e(TAG, "onIabPurchaseFinished - Fail - " + result.getMessage());
                    mPurchaseListener.onPurchaseFailed(skuName, result.getMessage());
                    return;
                }

                if (purchase != null && purchase.getSku().equals(skuName)) {
                    LLog.e(TAG, "onIabPurchaseFinished - Success");
                    mPurchaseListener.onPurchaseSuccessful(skuName);
                }
            }
        }, "");
    }

    public void startSetup(Context context, String hash) {
        mHelper = new IabHelper(context, hash);
        mHelper.startSetup(this);
    }

    public void consumeAllSku(final OnConsumeListener onConsumeListener) {
        LLog.e(TAG, "consumeAllSku");
        mHelper.queryInventoryAsync(true, new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                LLog.e(TAG, "consumeAllSku - onQueryInventoryFinished");
                for (final String s : mSkus) {
                    if (inventory.getSkuDetails(s) != null) {
                        mHelper.consumeAsync(inventory.getPurchase(s), new IabHelper.OnConsumeFinishedListener() {
                            @Override
                            public void onConsumeFinished(Purchase purchase, IabResult result) {
                                LLog.e(TAG, "consumeAllSku - onConsumeFinished - " + result.getMessage());
                                if (onConsumeListener != null) {
                                    onConsumeListener.onConsumeFinished(s, result.isSuccess(), result.getMessage());
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    public void consumeSku(final String sku, final OnConsumeListener onConsumeListener) {
        LLog.e(TAG, "consumeAllSku");
        mHelper.queryInventoryAsync(true, new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                LLog.e(TAG, "consumeAllSku - onQueryInventoryFinished");
                if (inventory.getSkuDetails(sku) != null) {
                    mHelper.consumeAsync(inventory.getPurchase(sku), new IabHelper.OnConsumeFinishedListener() {
                        @Override
                        public void onConsumeFinished(Purchase purchase, IabResult result) {
                            LLog.e(TAG, "consumeAllSku - onConsumeFinished - " + result.getMessage());
                            if (onConsumeListener != null) {
                                onConsumeListener.onConsumeFinished(sku, result.isSuccess(), result.getMessage());
                            }
                        }
                    });
                }
            }
        });
    }

    /*************************************
     * PRIVATE METHODS
     *************************************/
    private SkuDetails createTestSkuDetails() {
        LLog.e(TAG, "createTestSkuDetails");
        return new SkuDetails(
                "testItemType",
                SKU_TEST,
                "testType",
                "0.99",
                "Test Product",
                "Test product description",
                ""
        );
    }

    /*************************************
     * PUBLIC INTERFACES
     *************************************/
    public interface PurchaseListener {

        void onPurchaseSuccessful(String skuName);

        void onPurchaseFailed(String skuName, String message);

    }

    public interface InventoryListener {

        void onInventorySuccessful(String skuName, SkuDetails skuDetails, boolean isPurchased);

        void onInventoryFinished();

        void onInventoryFailed(String skuName);

    }

    public interface OnConsumeListener {

        void onConsumeFinished(String sku, boolean isSuccess, String message);

    }

}

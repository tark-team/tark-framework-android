package com.tarrk.framework.android.font;

import android.content.Context;
import android.graphics.Typeface;
import com.tarrk.framework.android.debug.LLog;

import java.lang.reflect.Field;

/**
 * User: Pavel
 * Date: 27.02.2015
 * Time: 17:02
 */
public class FontOverrideUtil {

    private final static String TAG = FontOverrideUtil.class.getSimpleName();

    public static void setDefaultFont(Context context, String staticTypefaceFieldName, String fontAssetName) {
        final Typeface regular = Typeface.createFromAsset(context.getAssets(), fontAssetName);

        replaceFont(staticTypefaceFieldName, regular);
    }

    protected static void replaceFont(String staticTypefaceFieldName, final Typeface newTypeface) {
        try {
            final Field staticField = Typeface.class.getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, newTypeface);
        } catch (NoSuchFieldException e) {
            LLog.e(TAG, "NoSuchFieldException - " + e);
        } catch (IllegalAccessException e) {
            LLog.e(TAG, "IllegalAccessException - " + e);
        }

    }

}

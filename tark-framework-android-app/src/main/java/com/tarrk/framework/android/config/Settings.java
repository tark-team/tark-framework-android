package com.tarrk.framework.android.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Settings {

    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;

    // --- INIT ---

    public static void init(Context context) {
        if (preferences == null) {
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
            editor = preferences.edit();
        }
    }

    // --- GET ---

    public static boolean getBoolean(String key, boolean value) {
        return preferences.getBoolean(key, value);
    }

    public static boolean getBoolean(String key) {
        return getBoolean(key, false);
    }

    public static float getFloat(String key, float value) {
        return preferences.getFloat(key, value);
    }

    public static float getFloat(String key) {
        return getFloat(key, 0f);
    }

    public static int getInt(String key, int value) {
        return preferences.getInt(key, value);
    }

    public static int getInt(String key) {
        return getInt(key, 0);
    }

    public static long getLong(String key, long value) {
        return preferences.getLong(key, value);
    }

    public static long getLong(String key) {
        return getLong(key, 0l);
    }

    public static String getString(String key, String value) {
        return preferences.getString(key, value);
    }

    public static String getString(String key) {
        return getString(key, "");
    }

    // --- PUT ---

    public static void putBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void putFloat(String key, float value) {
        editor.putFloat(key, value);
        editor.apply();
    }

    public static void putInt(String key, int value) {
        editor.putInt(key, value);
        editor.apply();
    }

    public static void putLong(String key, long value) {
        editor.putLong(key, value);
        editor.apply();
    }

    public static void putString(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    // --- OTHER ---

    public static void toggle(String key) {
        setEnabled(key, !isEnabled(key));
    }

    public static boolean isEnabled(String key) {
        return getBoolean(key);
    }

    public static void setEnabled(String preferenceKey, boolean b) {
        editor.putBoolean(preferenceKey, b).apply();
    }

    public static void registerPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        preferences.registerOnSharedPreferenceChangeListener(listener);
    }

    public static void unregisterPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        preferences.unregisterOnSharedPreferenceChangeListener(listener);
    }

    SharedPreferences.OnSharedPreferenceChangeListener spChanged = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                              String key) {
            // your stuff here
        }
    };

}
package com.tarrk.framework.android.font;

import android.graphics.Typeface;
import android.widget.TextView;

/**
 * User: Pavel
 * Date: 18.03.2015
 * Time: 16:30
 */
public class TypefaceUtil {

    public static void initRoboto(TextView v) {
        initFont(v, "Roboto-Regular.ttf");
    }

    public static void initRobotoBold(TextView v) {
        initFont(v, "Roboto-Bold.ttf");
    }

    public static void initRobotoCondensed(TextView v) {
        initFont(v, "fonts/RobotoCondensed-Regular.ttf");
    }

    public static void initRobotoCondensedLight(TextView v) {
        initFont(v, "fonts/RobotoCondensed-Light.ttf");
    }

    public static void initRobotoCondensedBold(TextView v) {
        initFont(v, "fonts/RobotoCondensed-Bold.ttf");
    }

    public static void initRobotoMedium(TextView v) {
        initFont(v, "fonts/Roboto-Medium.ttf");
    }

    public static void initRobotoLight(TextView v) {
        /*initFont(v, "fonts/Roboto-Light.ttf");*/
        initFont(v, "Roboto-Light.ttf");
    }

    public static void initFont(TextView v, String fontName) {
        Typeface font = Typeface.createFromAsset(v.getContext().getAssets(), fontName);
        v.setTypeface(font);
    }

}

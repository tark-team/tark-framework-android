package com.tarrk.framework.android.gui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import com.tarrk.framework.android.font.TypefaceUtil;

public class RobotoMediumButton extends Button {

    public RobotoMediumButton(Context context) {
        super(context);
        TypefaceUtil.initRobotoMedium(this);
    }

    public RobotoMediumButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypefaceUtil.initRobotoMedium(this);
    }

    public RobotoMediumButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypefaceUtil.initRobotoMedium(this);
    }

}

package com.tarrk.framework.android.gui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;

public class ScreenUtil {

    public static Point getScreenSize(Activity a) {

        Display display = a.getWindowManager().getDefaultDisplay();
        final Point point = new Point();

        try {
            display.getSize(point);
        } catch (NoSuchMethodError ignore) {
            point.x = display.getWidth();
            point.y = display.getHeight();
        }

        return point;

    }

    public static Point getRealScreenSize(Activity a) {
        Display display = a.getWindowManager().getDefaultDisplay();
        final Point point = new Point();

        try {
            display.getRealSize(point);
        } catch (NoSuchMethodError ignore) {
            /**
             * Possible bug
             * because we need here REAL size
             */
            point.x = display.getWidth();
            point.y = display.getHeight();
        }

        return point;

    }

    public static Point getRealScreenSizeInDp(Context context) {
        //Debug.log(TAG, "getRealScreenSizeInDp");
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return new Point(
                (int)(displayMetrics.heightPixels / displayMetrics.density),
                (int)(displayMetrics.widthPixels / displayMetrics.density)
        );
    }

    public static int x(Activity a) {
        return getScreenSize(a).x;
    }

    public static int y(Activity a) {
        return getScreenSize(a).y;
    }

    public static int getStatusBarHeight(Context c) {
        int result = 0;
        int resourceId = c.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = c.getResources().getDimensionPixelSize(resourceId);
        }

        return result;
    }

    public static int dpToPx(Context c, int dp) {
        DisplayMetrics displayMetrics = c.getResources().getDisplayMetrics();
        return (int) ((dp * displayMetrics.density) + 0.5);
    }

    public static int dpToPx(Context c, float dp) {
        DisplayMetrics displayMetrics = c.getResources().getDisplayMetrics();
        return (int) ((dp * displayMetrics.density) + 0.5);
    }

}

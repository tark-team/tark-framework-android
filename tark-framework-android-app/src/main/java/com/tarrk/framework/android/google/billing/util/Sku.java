package com.tarrk.framework.android.google.billing.util;

/**
 * User: Pavel
 * Date: 10.06.2015
 * Time: 18:16
 */
public class Sku {

    private String name;
    private Type type;
    private ActionListener actionListener;

    public enum Type {MANAGED, SUBSCRIPTION}

    public Sku(String name, Type type, ActionListener actionListener) {
        this.name = name;
        this.type = type;
        this.actionListener = actionListener;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public ActionListener getActionListener() {
        return actionListener;
    }

    // --

    public interface ActionListener {

        void onInventory(boolean alreadyPurchased);

        void onPurchase(boolean isSuccess);

        void onConsume(boolean isSuccess);

    }

}

package com.tarrk.framework.android.gui;

import android.graphics.Color;

/**
 * User: Pavel
 * Date: 09.04.2015
 * Time: 13:05
 */
public class ColorUtil {

    public static int setAlpha(float alpha, int color) {

        int mAlpha = (int) (255f * alpha);

        return Color.argb(
                mAlpha,
                Color.red(color),
                Color.green(color),
                Color.blue(color)
        );
    }

}

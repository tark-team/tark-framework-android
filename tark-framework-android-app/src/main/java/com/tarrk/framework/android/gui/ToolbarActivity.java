package com.tarrk.framework.android.gui;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tarrk.framework.android.R;

public abstract class ToolbarActivity extends AppCompatActivity {

    /*************************************
     * BINDS
     *************************************/
    protected Toolbar mToolbar;
    protected TextView mToolbarTitle;
    protected View mToolbarShadow;

    /*************************************
     * PROTECTED METHODS
     *************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        initViews();
        addActivityContent();
        initToolbar();

    }

    @LayoutRes
    abstract protected int getContentLayoutRes();

    @LayoutRes
    protected int getToolbarLayoutRes() {
        return 0;
    }

    protected String getToolbarTitle() {

        return getTitle().toString();
    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mToolbarShadow = findViewById(R.id.toolbar_shadow);
    }

    private void addActivityContent() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View childLayout = inflater.inflate(getContentLayoutRes(), (ViewGroup) findViewById(R.id.root));

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );
        params.addRule(RelativeLayout.BELOW, R.id.toolbar);

        addContentView(childLayout, params);
    }

    private void initToolbar() {
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        mToolbarTitle.setText(getToolbarTitle());
    }

}

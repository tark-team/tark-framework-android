package com.tarrk.framework.android.debug;

import android.text.TextUtils;
import android.util.Log;

import com.tarrk.framework.android.io.AbstractFile;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TLog extends AbstractFile {

    public static final String TAG = "tark";

    /*************************************
     * PRIVATE FIELDS
     *************************************/
    private static final String LOG_TIME_STAMP_PATTERN = "yyyy.MM.dd HH:mm:ss";
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    private static final String LOG_MESSAGE_SEPARATOR = "----------------";

    private static final String LOG_FILE_NAME = "log.txt";
    private static final long MAX_FILE_SIZE = 512 * 1024;    //0.5 mb

    /*************************************
     * PRIVATE FIELDS
     *************************************/
    private static volatile TLog sInstance;
    private boolean mBuildConfigDebug;

    protected TLog() {
        createFile();
    }

    /*************************************
     * PUBLIC METHODS
     *************************************/
    public static TLog getInstance() {
        if (sInstance == null) {
            synchronized (TLog.class) {
                if (sInstance == null) {
                    sInstance = new TLog();
                }
            }
        }
        return sInstance;
    }

    /**
     * Call this in onCreate of Application class like
     * TLog.getInstance().init(BuildConfig.DEBUG);
     */
    public void init(boolean buildConfigDebug) {
        mBuildConfigDebug = buildConfigDebug;
    }

    public void writeLine(String line) {
        write(line);
    }

    public void writeMethodName() {
        write("");
    }

    public void writeErrorLine(Exception exception) {
        if (mBuildConfigDebug && createFile()) {
            writeExceptionMessage(exception);
            writeExceptionStackTrace(exception);
        }
    }

    /*************************************
     * PROTECTED METHODS
     *************************************/
    @Override
    protected String getFileName() {
        return LOG_FILE_NAME;
    }

    @Override
    protected long getFileMaxSize() {
        return MAX_FILE_SIZE;
    }

    /*************************************
     * PRIVATE METHODS
     *************************************/
    private void write(String message) {
        if (mBuildConfigDebug && createFile()) {
            String wrappedMessage = wrapMessage(message);
            writeFile(wrappedMessage, true);
            Log.e(TAG, wrappedMessage);
        }
    }

    private void writeExceptionMessage(Exception exception) {
        if (!TextUtils.isEmpty(exception.getMessage())) {
            String wrappedMessage = wrapMessage(exception.getMessage());
            writeFile(wrappedMessage, true);
        }
    }

    private void writeExceptionStackTrace(Exception exception) {
        PrintStream printStream = null;
        FileOutputStream logFileStream = null;

        try {
            logFileStream = new FileOutputStream(mFile, true);
            printStream = new PrintStream(logFileStream);
            printStream.write(wrapMessage("").getBytes());
            exception.printStackTrace(printStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeLogFileStream(logFileStream);
            closePrintStream(printStream);
        }
    }

    private void closeLogFileStream(FileOutputStream logFileStream) {
        try {
            if (logFileStream != null) {
                logFileStream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closePrintStream(PrintStream printStream) {
        try {
            if (printStream != null) {
                printStream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String wrapMessage(String message) {
        String resultMessage = "";
        String currentTimeStamp = getDateFormat().format(new Date());

        resultMessage += currentTimeStamp + LINE_SEPARATOR;
        resultMessage += getMethodLocation() + LINE_SEPARATOR;

        resultMessage += message + LINE_SEPARATOR;
        resultMessage += LOG_MESSAGE_SEPARATOR + LINE_SEPARATOR;

        return resultMessage;
    }

    protected SimpleDateFormat getDateFormat() {
        return new SimpleDateFormat(
                LOG_TIME_STAMP_PATTERN,
                Locale.UK);
    }

    private static String getMethodLocation() {
        final String className = TLog.class.getName();
        final StackTraceElement[] traces = Thread.currentThread().getStackTrace();
        boolean found = false;

        for (int i = 0; i < traces.length; i++) {
            StackTraceElement trace = traces[i];

            try {
                if (found) {
                    if (!trace.getClassName().startsWith(className)) {
                        Class<?> clazz = Class.forName(trace.getClassName());
                        return "[" + getClassName(clazz)
                                + ":" + trace.getMethodName()
                                + ":" + trace.getLineNumber() + "]: ";
                    }
                } else if (trace.getClassName().startsWith(className)) {
                    found = true;
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return "[]: ";
    }

    private static String getClassName(Class<?> clazz) {
        if (clazz == null) {
            return "";
        }

        if (!TextUtils.isEmpty(clazz.getSimpleName())) {
            return clazz.getSimpleName();
        }

        return getClassName(clazz.getEnclosingClass());
    }

}
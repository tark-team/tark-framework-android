package com.tarrk.framework.android.math;

/**
 * User: Pavel
 * Date: 10.04.2015
 * Time: 21:39
 */
public class MathUtil {

    public float roundToSecondDecimalFiveEnded(float number){
        return Math.round(number * 20) / 20;
    }

}

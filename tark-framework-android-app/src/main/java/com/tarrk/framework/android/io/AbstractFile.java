package com.tarrk.framework.android.io;

import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import com.tarrk.framework.android.debug.TLog;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

public abstract class AbstractFile {

    protected File mFile;

    /**
     * without path
     */
    protected abstract String getFileName();

    /**
     * @return path of file parent directory on external storage (SD card)
     */
    protected String getParentDirectory() {
        return Environment
                .getExternalStorageDirectory()
                .getPath() + "/"
                + getAppDirectoryName();
    }

    /**
     * do not forget "/" at the end
     */
    protected String getAppDirectoryName() {
        return ".tark/";
    }

    /**
     * if actual file size is bigger than this value then file is recreated.
     * Returns 0 by default, which means we not limit file size.
     *
     * used for example to limit size of log file, which has a lot of logs during development
     * @return the maximum number of bytes in this file.
     */
    protected long getFileMaxSize() {
        return 0;
    }

    protected void checkRecreateFile() {
        if(!isExist()) {
            createFile();
            return;
        }

        if (mFile.length() >= getFileMaxSize()
                && mFile.delete()) {
            createFile();
        }
    }

    public boolean createFile() {
        try {
            return ensureParentDirectory()
                    && ensureFile();

        } catch (IOException e) {
            Log.e(TLog.TAG, "failed while creating log file");
            return false;
        }
    }

    protected boolean ensureParentDirectory() {
        File parentDirectory = new File(getParentDirectory());
        return parentDirectory.exists()
                || parentDirectory.mkdirs();
    }

    protected boolean ensureFile() throws IOException {
        String fullFileName = getParentDirectory() + getFileName();
        mFile = new File(fullFileName);

        return mFile.exists() || mFile.createNewFile();
    }

    protected boolean isExist() {
        return mFile != null && mFile.exists();
    }

    public void writeFile(String line, boolean append) {
        if (TextUtils.isEmpty(line)) {
            return;
        }

        checkRecreateFile();
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(mFile, append);
            fos.write(line.getBytes());

            fos.flush();
            fos.close();
        } catch (IOException e) {
            TLog.getInstance().writeErrorLine(e);
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (Exception e) {
                TLog.getInstance().writeErrorLine(e);
            }
        }
    }

    public String readFile() {
        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(mFile));
            StringBuilder builder = new StringBuilder("");
            String buffer;

            while ((buffer = br.readLine()) != null) {
                builder.append(buffer);
            }

            return builder.toString();
        } catch (Exception e) {
            TLog.getInstance().writeErrorLine(e);
            return null;
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (Exception exc) {
                TLog.getInstance().writeErrorLine(exc);
            }
        }
    }

}
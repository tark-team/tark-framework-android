package com.tarrk.framework.android.gui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tarrk.framework.android.font.TypefaceUtil;

public class RobotoCondensedBoldTextView extends TextView {

    public RobotoCondensedBoldTextView(Context context) {
        super(context);
        TypefaceUtil.initRobotoCondensedBold(this);
    }

    public RobotoCondensedBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypefaceUtil.initRobotoCondensedBold(this);
    }

    public RobotoCondensedBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypefaceUtil.initRobotoCondensedBold(this);
    }

}

package com.tarrk.framework.android.gui;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;

/**
 * User: Pavel
 * Date: 19.03.2015
 * Time: 21:25
 */
public class BitmapEffectsUtil {

    public static Bitmap blur(Context c, Bitmap b, float blurRadius) {

        if (Build.VERSION.SDK_INT >= 17) {

            RenderScript rs = RenderScript.create(c);

            final Allocation input = Allocation.createFromBitmap(rs, b); //use this constructor for best performance, because it uses USAGE_SHARED mode which reuses memory
            final Allocation output = Allocation.createTyped(rs, input.getType());
            final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
            script.setRadius(blurRadius);
            script.setInput(input);
            script.forEach(output);
            output.copyTo(b);
        }

        return b;

    }

}

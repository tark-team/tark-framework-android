package com.tarrk.framework.android.power;

import android.app.KeyguardManager;
import android.content.Context;
import android.os.PowerManager;

/**
 * User: Pavel
 * Date: 19.11.2015
 * Time: 10:35
 */
public class PowerUtils {

    public static void wakeUpTheScreen(Context context) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
        wakeLock.acquire();
    }

    public static void disableLock(Context context) {
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("TAG");
        keyguardLock.disableKeyguard();
    }

}

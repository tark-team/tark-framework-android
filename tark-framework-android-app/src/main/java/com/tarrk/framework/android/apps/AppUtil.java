package com.tarrk.framework.android.apps;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;

/**
 * User: Pavel
 * Date: 22.05.2015
 * Time: 16:50
 */
public class AppUtil {

    /*************************************
     * PUBLIC STATIC CONSTANTS
     *************************************/
    public static final String MANUFACTURER_HUAWEI = "huawei";
    public static final String MANUFACTURER_ARCHOS = "archos";
    public static final String MANUFACTURER_MOTOROLA = "motorola";
    public static final String MODEL_HUAWEI_ALE = "ale";
    public static final String MODEL_HUAWEI_GRA = "gra";
    public static final String MODEL_ARCHOS_HUDL = "hudl";
    public static final String MODEL_MOTO_G = "moto g";

    public static boolean isAppInstalled(Context context, String packageName) {
        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static void runApp(Context c, String packageName) {
        c.startActivity(c.getPackageManager().getLaunchIntentForPackage(packageName));
    }

    public static void installApp(Context c, String packageName) {
        c.startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(String.format("%s%s", "market://details?id=", packageName))));
    }

    public static void runOrInstallApp(Context c, String packageName) {
        if (isAppInstalled(c, packageName)) {
            runApp(c, packageName);
        } else {
            installApp(c, packageName);
        }
    }

    public static String getVersionName(Context c) {
        try {
            return c.getPackageManager().getPackageInfo(c.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "PackageManager.NameNotFoundException";
        }
    }

    public static int getVersionCode(Context c) {
        try {
            return c.getPackageManager().getPackageInfo(c.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static boolean isHuaweiManufacturer() {
        // TODO: move to tark framework
        String manufacturer = Build.MANUFACTURER.toLowerCase();
        String model = Build.MODEL.toLowerCase();

        return manufacturer.equals(MANUFACTURER_HUAWEI)
                || manufacturer.equals(MANUFACTURER_ARCHOS)
                || manufacturer.contains(MANUFACTURER_HUAWEI)
                || manufacturer.contains(MANUFACTURER_ARCHOS)
                || manufacturer.contains(MODEL_HUAWEI_ALE)
                || manufacturer.contains(MODEL_HUAWEI_GRA)
                || model.contains(MODEL_HUAWEI_ALE)
                || model.contains(MODEL_HUAWEI_GRA)
                || model.contains(MODEL_ARCHOS_HUDL)
                || (manufacturer.contains(MANUFACTURER_MOTOROLA) && model.contains(MODEL_MOTO_G));
    }
}
